# This file will be patched by setup.py
# The __version__ should be set to the branch name
# (e.g. "trunk" or "0.4.x")

# You MUST use double quotes (so " and not ')

__version__ = "0.7.19"
__baseline__ = "46155d04cd1d52165de042bd212c47facf606ae9 (not committed)"
