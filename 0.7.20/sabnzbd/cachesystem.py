#!/usr/bin/python -OO

"""
sabnzbd.cache - caching system master and slaves functionality
"""

import re
import logging
import sabnzbd.downloader
import sabnzbd.nzbqueue
import sabnzbd.constants
from sabnzbd.newswrapper import NewsWrapper


class CacheSystem(object):
    """ Singleton aggregates all Caching System logic and master/slaves managing
    """
    do = None

    def __init__(self):
        assert sabnzbd.downloader.Downloader.do is not None

        # regexp for server name that will trigger Caching System
        self.server_regexp_for_cache_enable = ".+\.snelnl\.com|^news\.sslusenet\.com"
        # MASTER
        self.master_hostname = "cache.snelnl.com"
        # max number of articles requested from master and unprocessed by slaves yet
        # to prevent spam of master server by request all articles at once
        self.unprocessed_articles_limit = 50

        self.servers = sabnzbd.downloader.Downloader.do.servers
        self.slaves = []
        self.slave_threads_limit = 0
        self.last_template_server = None
        self.master = None
        # used to calc unprocessed_articles_limit
        self.articles_assigned = []

        self.update_servers()

        CacheSystem.do = self

    def update_servers(self):
        """ Check servers list and find snelnl.com ones. Update them to be template servers.
            Add master if it does not exist or schedule update master and slaves if they are
            added already.
        """
        if sabnzbd.LOG_ALL:
            logging.debug("Update cache servers")
        self.last_template_server = None
        self.slave_threads_limit = 0

        for s in self.servers:
            if re.compile(self.server_regexp_for_cache_enable, re.IGNORECASE).match(s.host):
                if s.active and s.host and s.port and s.threads and not (s.slave or s == self.master):
                    s.template_server = True
                    self.last_template_server = s

        if self.last_template_server:
            logging.debug('Cache system triggered on: %s', self.last_template_server)
            self.slave_threads_limit = self.last_template_server.threads

            if sabnzbd.LOG_ALL:
                logging.debug('Max number of slave threads: %s', self.slave_threads_limit)

        self.master = self.__update_downloader_with_new_master()
        self.__update_downloader_with_new_slaves()

    def slave_threads_limit_reached(self):
        """ Returns true if slaves common threads limit is reached.
        """
        slave_threads = 0
        for slave in self.slaves:
            slave_threads += len(slave.busy_threads)
        return slave_threads >= self.slave_threads_limit

    def debug_cache_threads(self):
        connected_n_busy = 0
        connected_n_idle = 0
        for slave in self.slaves:
            for nw in slave.busy_threads:
                if nw.connected:
                    connected_n_busy += 1
            for nw in slave.idle_threads:
                if nw.connected:
                    connected_n_idle += 1
        logging.debug('Number of open connections by SLAVES: busy %s idle %s',
                      connected_n_busy, connected_n_idle)

    def redirect_article_download(self, nw):
        """ Call it when master respond with 430 code. It closes master thread,
            put master to try list, and remove particular slave from try list.
        """

        def get_slave_hostname_from_master_response(line):
            if len(line) > 12:
                if line[4:12] == 'Slave = ':
                    return line[12:]
            return None

        assert isinstance(nw, NewsWrapper)
        assert nw.article is not None and nw.server is not None
        assert len(nw.lines) == 1
        assert self.master == nw.server

        article = nw.article
        response = nw.lines[0]

        slave_hostname = get_slave_hostname_from_master_response(response)
        if slave_hostname is None:
            logging.debug('Response from master cache: %s. Skip redirecting.', response)
            return False

        if sabnzbd.LOG_ALL:
            logging.debug('Thread %s@%s: article %s downloading redirect to %s',
                          nw.thrdnum, nw.server, article.article, slave_hostname)

        if not self.__assign_article_to_slave(slave_hostname, article):
            self.article_redirect_from_slaves(article)

        # master try should not be counted
        article.tries -= 1
        # reset fetcher for slave processing (it will not be processed before fetcher is empty)
        article.fetcher = None
        # put to try list, so it will not be processed the second time for master
        article.add_to_try_list(self.master)
        # free master connection
        self.master.bad_cons = 0  # Succesful data, clear "bad" counter
        nw.soft_reset()
        self.master.busy_threads.remove(nw)
        self.master.idle_threads.append(nw)
        return True

    def article_redirect_from_slaves(self, article):
        if not article:
            return

        article.allow_template_server = True
        self.remove_templates_from_try_lists(article)
        if article in self.articles_assigned:
            self.articles_assigned.remove(article)

        if sabnzbd.LOG_ALL:
            logging.debug(
                "%s REDIRECT from slave. Done: %s. In master try list: %s",
                article.article, article not in article.nzf.articles, article.server_in_try_list(self.master))
            fetcher = article.fetcher and article.fetcher.host
            logging.debug(
                "Fetcher (%s) Not in try list (%s) Not in F try list (%s) Not in O try list (%s)",
                fetcher, article.slave_not_in_try_list() is not None,
                article.nzf.slave_not_in_try_list() is not None,
                article.nzf.nzo.slave_not_in_try_list() is not None)

    def refresh_unprocessed_articles(self):
        """ Redirect articles that are processed by cache to template servers
        """
        def article_redirect_back_to_slave(art):
            if sabnzbd.LOG_ALL:
                logging.debug(
                    "%s BACK to slave. Done: %s. In master try list: %s. Allow template: %s",
                        article.article, article not in article.nzf.articles, article.server_in_try_list(self.master),
                    article.allow_template_server)
                fetcher = article.fetcher and article.fetcher.host
                logging.debug(
                    "Fetcher (%s) Art try list (%s) F try list (%s) O try list (%s) All try list: %s",
                    fetcher, article.slave_not_in_try_list() is None,
                    article.nzf.slave_not_in_try_list() is None,
                    article.nzf.nzo.slave_not_in_try_list() is None,
                    sabnzbd.nzbqueue.NzbQueue.do.slave_not_in_try_list() is None)

            slave = art.slave_not_in_try_list()
            art.nzf.remove_from_try_list(slave)
            art.nzf.nzo.remove_from_try_list(slave)
            sabnzbd.nzbqueue.NzbQueue.do.remove_from_try_list(slave)


        if not self.articles_assigned:
            return

        for article in self.articles_assigned[:]:
            # remove from list if article is processed or not assigned to any slave
            if article not in article.nzf.articles or \
                    not article.slave_not_in_try_list() or \
                    article.nzf.deleted or article.nzf.nzo.deleted:
                self.article_redirect_from_slaves(article)
            elif not article.fetcher:
                if not article.nzf.slave_not_in_try_list() or \
                        not article.nzf.nzo.slave_not_in_try_list() or \
                        not sabnzbd.nzbqueue.NzbQueue.do.slave_not_in_try_list():
                    if article.tries < 2:
                        article_redirect_back_to_slave(article)
                        article.tries += 1
                    else:
                        article.tries = 0
                        self.article_redirect_from_slaves(article)

    def is_unprocessed_articles_limit_reached(self):
        """ Check the number of articles that are assigned, but is not processed by slaves yet.
        """

        if not self.articles_assigned:
            return False

        unprocessed = len(self.articles_assigned)
        # do not calculate articles that are currently processing by slaves or on pause
        for article in self.articles_assigned[:]:
            if (article.fetcher and article.fetcher in self.slaves) or \
                    article.nzf.nzo.status in (sabnzbd.constants.Status.PAUSED, sabnzbd.constants.Status.GRABBING):
                unprocessed -= 1

        if sabnzbd.LOG_ALL and 0 < unprocessed < self.unprocessed_articles_limit:
            logging.debug("Number of unprocessed articles: %s", unprocessed)

        return unprocessed >= self.unprocessed_articles_limit

    def is_cache_server(self, server):
        return server == self.master or server.slave

    def remove_templates_from_try_lists(self, article=None):
        """ Allow template servers to get articles, but do not clean articles try list,
        clean it only for queue, nzo and nzf
        """
        for server in self.servers:
            if server.template_server:
                if article is None:
                    sabnzbd.nzbqueue.NzbQueue.do.remove_from_try_lists(server)
                else:
                    article.nzf.remove_from_try_list(server)
                    article.nzf.nzo.remove_from_try_list(server)
                    sabnzbd.nzbqueue.NzbQueue.do.remove_from_try_list(server)

    def __update_downloader_with_new_master(self):
        """ Add new master to servers list or schedule update.
        """
        # it can be None if last_template_server is not specified
        new_master_instance = self.__get_new_master_instance()

        if self.master:
            self.__schedule_restart(self.master, new_master_instance)
            return self.master

        if not new_master_instance:
            return None

        # check user does not add master to servers list by himself
        # if it is added then it will be replaced
        for srv in self.servers:
            if srv.id == new_master_instance.id:
                self.__schedule_restart(srv, new_master_instance)
                return srv

        self.servers.append(new_master_instance)
        return new_master_instance

    def __update_downloader_with_new_slaves(self):
        """ Schedule update of all slaves if any.
        """
        if self.slaves:
            for slave in self.slaves[:]:
                new_slave_instance = self.__get_new_slave_instance(slave.host)
                self.__schedule_restart(slave, new_slave_instance)

    def __get_new_master_instance(self):
        """ Check if there are "*.snelnl.com" servers in the list of server.
            Returns master server instance
        """
        ts = self.last_template_server
        if not (ts and ts.active and ts.host and ts.port and ts.threads):
            return None

        hostname = self.master_hostname
        if ts.ssl:
            port = 563
        else:
            port = 119
        port = ts.port
        master_id = "{}:{}".format(hostname, port)
        timeout = ts.timeout
        threads = 2
        fillserver = False
        ssl = ts.ssl
        username = ts.username
        password = ts.password
        optional = True
        retention = ts.retention
        master = sabnzbd.downloader.Server(master_id, hostname, port, timeout, threads, fillserver, ssl,
                                           username, password, optional, retention)

        if sabnzbd.LOG_ALL: logging.debug('New master %s instance initialized', master.id)

        return master

    def __get_new_slave_instance(self, hostname):
        """ Returns new slave instance
        """
        ts = self.last_template_server
        if not (ts and ts.active and ts.host and ts.port and ts.threads):
            return None

        if ts.ssl:
            port = 563
        else:
            port = 119
        port = ts.port
        slave_id = "{}:{}".format(hostname, port)
        timeout = ts.timeout
        threads = ts.threads
        fillserver = False
        ssl = ts.ssl
        username = ts.username
        password = ts.password
        optional = True
        retention = ts.retention
        slave = sabnzbd.downloader.Server(slave_id, hostname, port, timeout, threads, fillserver, ssl,
                                          username, password, optional, retention)
        slave.slave = True

        if sabnzbd.LOG_ALL: logging.debug('New slave %s instance initialized', slave.id)

        return slave

    def __assign_article_to_slave(self, hostname, article):
        """ Remove slave from article try list, i.e. schedule it for downloading.
            Add new slave to servers list if slave is new one.
        """
        slave = None
        for s in self.slaves:
            if s.host == hostname:
                slave = s
                break

        if slave is None:
            slave = self.__get_new_slave_instance(hostname)
            self.servers.append(slave)
            self.slaves.append(slave)
        else:
            if article.is_server_fully_disabled(slave):
                logging.debug("Cannot assign to slave. %s active: %s threads: %s", slave, slave.active, slave.threads)
                return False

        article.remove_from_try_list(slave)
        article.nzf.remove_from_try_list(slave)
        article.nzf.nzo.remove_from_try_list(slave)
        sabnzbd.nzbqueue.NzbQueue.do.remove_from_try_list(slave)

        if article not in self.articles_assigned:
            self.articles_assigned.append(article)
        return True

    @staticmethod
    def __schedule_restart(old_server, new_server):
        """ Schedule update server from old_server to new_server
        """
        assert isinstance(old_server, sabnzbd.downloader.Server)
        assert new_server is None or isinstance(new_server, sabnzbd.downloader.Server)

        old_server.new_server = new_server
        old_server.restart = True
        sabnzbd.downloader.Downloader.do.increase_restart_count()
        if sabnzbd.LOG_ALL: logging.debug('Scheduled to be restarted: %s', old_server)
